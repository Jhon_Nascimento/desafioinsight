Desafio 

1 a) *Encontrar o jogador de Futebol Ronaldo Luís Nazário de Lima;
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dbo:  <http://dbpedia.org/ontology/>

SELECT ?player_name

FROM <http://dbpedia.org/>

WHERE { 
?group rdf:type dbo:SoccerPlayer .
?group rdfs:label ?player_name .
FILTER REGEX(?player_name, "Ronaldo Luís Nazário de Lima")
}

b) *Listar os times que o Ronaldo jogou
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dbo:  <http://dbpedia.org/ontology/>

SELECT ?player_name ?team

FROM <http://dbpedia.org/>

WHERE { 
?group rdf:type dbo:SoccerPlayer .
?group rdfs:label ?player_name .
?group dbo:team ?teamPlayer .
?teamPlayer rdfs:label ?team
FILTER REGEX(?player_name, "Ronaldo Luís Nazário de Lima") .
FILTER (LANG(?team)="en") .
}

c)
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dbo:  <http://dbpedia.org/ontology/>
PREFIX dbr:     <http://dbpedia.org/resource/>

SELECT ?player_name ?champion

FROM <http://dbpedia.org/>

WHERE {
  ?group rdf:type dbo:SoccerPlayer .
  ?group rdfs:label ?player_name .
  ?player dbp:leagueTopscorer dbr:Ronaldo .
  ?player rdfs:label ?champion .
  FILTER (LANG(?champion)="en") .
  FILTER REGEX(?player_name, "Ronaldo Luís Nazário de Lima") .
} Order by ?champion
