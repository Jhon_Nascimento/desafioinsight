import pandas as pd

def multipleReplace(text):
    return "".join(["" if char in ".()-_/\&;:!?,\"" else char for char in text])

teste = pd.read_excel('"C:\\Users\\jh0nn\\Documents\\TesteRasas\\rasa_ontologias_exemplo-master\\rel_atend_131393.xls')     
nameRow = ""
for index, row in teste.iterrows():
    nameRow += "        - "
    nameRow += (multipleReplace(row['nom_servico'])).lower().capitalize()
    nameRow += "\n"
    nameRow += "        - "
    nameRow += (multipleReplace(row['nom_servico'])).lower()
    nameRow += "\n"
    nameRow += "        - "
    nameRow += (multipleReplace(row['nom_servico'])).upper()
    nameRow += "\n"
    nameRow += "        - "
    nameRow += (multipleReplace(row['nom_servico'])).title()
    nameRow += "\n"
    

print(nameRow)
