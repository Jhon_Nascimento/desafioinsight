import sys
from ruamel.yaml import YAML

yaml = YAML()

curlyaml = open('./data/nlu.yml', 'r', encoding='utf-8')
data = yaml.load(curlyaml, Loader=yaml.FullLoader)

with open('./data/nlu6.yml', 'w', encoding = "utf-8") as yaml_file:
    dump = yaml.dump(data, default_flow_style = False, allow_unicode = True, encoding = None)
    yaml_file.write( dump )