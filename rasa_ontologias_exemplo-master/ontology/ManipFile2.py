import pandas as pd
import sys
from ruamel.yaml import YAML
import time

def multipleReplace(text):
    return "".join(["" if char in ".()_/\&;:~!?,\"" else char for char in text])


teste = pd.read_excel('./rel_atend_131393.xls')     
 
yaml = YAML()

curlyaml = open('./tests/test_nlu.yml', 'r', encoding='utf-8')
data = yaml.load(curlyaml)

newLookup = {'lookup': 'nomServico', 'examples': ''}

for index, row in teste.iterrows():
    nameRow = (multipleReplace(row['nom_servico'])).lower().capitalize()
    nameRow = nameRow.replace('ccedila', 'ç')
    nameRow = nameRow.replace('ccedilo', 'ç')
    nameRow = nameRow.replace('tildeo', 'ão')
    nameRow = nameRow.replace('tilde', 'õ')
    nameRow = nameRow.replace('acirc', 'â')
    nameRow = nameRow.replace('ocirc', 'ô')
    nameRow = nameRow.replace('ecirc', 'ê')
    nameRow = nameRow.replace('ndash ', '')
    nameRow = nameRow.replace('aacute', 'á')
    nameRow = nameRow.replace('eacute', 'é')
    nameRow = nameRow.replace('iacute', 'í')
    nameRow = nameRow.replace('oacute', 'ó')
    nameRow = nameRow.replace('uacute', 'ú')
    nameRow = nameRow.replace('2ordf', '2ª')
    nameRow = nameRow.replace('capitalinterior', 'capital e interior')
    nameRow = nameRow.replace('-', ' ')
    nameRow = '- ' + (nameRow.replace('  ', '')) + '\n'
    newLookup['examples'] += nameRow

for idx, (key, example) in enumerate(data['nlu']):
    if(key == 'lookup' and data['nlu'][idx][key] == 'nomServico'):
        print("entrou no if")
        data['nlu'][idx][example] += newLookup['examples']

with open('./tests/test_nlu.yml', 'w', encoding='utf-8') as f:
    yaml.dump(data, f) # Also note the safe_dump

#print(nameRow)