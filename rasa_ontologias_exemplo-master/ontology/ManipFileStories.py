import pandas as pd
import sys
from ruamel.yaml import YAML
import time

def multipleReplace(text):
    return "".join(["" if char in ".()-_/\&;:!?,\"" else char for char in text])


teste = pd.read_excel('./rel_atend_131393.xls')     
 
yaml = YAML()

curlyaml = open('./tests/test_stories.yml', 'r', encoding='utf-8')
data = yaml.load(curlyaml)
cont = 1

for index, row in teste.iterrows():
  nameRow = (multipleReplace(row['nom_servico'])).lower().capitalize()
  newLookup = {'story': '', 'steps': [{'user': '', 'intent': ''}, {'action': ''}]}
  newLookup['story'] += "question "+ cont
  newLookup['steps'][0]['user'] +=  "O cpf é um documento necessario para "+ nameRow +" da categoria saúde?"
  newLookup['steps'][0]['intent'] += "doc_neces"
  newLookup['steps'][1]['action'] += "action_create_query"
  
  data['stories'].append(newLookup)
  newLookup = {'story': '', 'steps': [{'user': '', 'intent': ''}, {'action': ''}]}
  newLookup['story'] += "question "+ cont
  newLookup['steps'][0]['user'] +=  "O cpf é um documento necessario para "+ nameRow +" da categoria saúde?"
  newLookup['steps'][0]['intent'] += "doc_neces"
  newLookup['steps'][1]['action'] += "action_create_query"
  
  data['stories'].append(newLookup)
  newLookup = {'story': '', 'steps': [{'user': '', 'intent': ''}, {'action': ''}]}
  newLookup['story'] += "question "+ cont
  newLookup['steps'][0]['user'] +=  "O cpf é um documento necessario para "+ nameRow +" da categoria saúde?"
  newLookup['steps'][0]['intent'] += "doc_neces"
  newLookup['steps'][1]['action'] += "action_create_query"
  
  data['stories'].append(newLookup)
  
  break

print(data)

with open('./tests/test_stories.yml', 'w', encoding='utf-8') as f:
    yaml.dump(data, f) # Also note the safe_dump
"""
#print(nameRow)
- story: question 50
   steps:
  - user: |
      O cpf é um documento necessario para serviços da categoria saúde?
    intent: doc_neces
  - action: action_create_query
  
- story: question 51
  steps:
  - user: |
      Preciso do cpf para solicitar serviços de saúde?
    intent: doc_neces
  - action: action_create_query
  
- story: question 52
  steps:
  - user: |
      Gostaria de saber se é necessário cpf para os serviços de saúde?
    intent: doc_neces
  - action: action_create_query
  
- story: question 53
  steps:
  - user: |
      Poderia me informar sobre os serviços de saúde que precisam de CPF?
    intent: doc_neces
  - action: action_create_query
  
- story: question 54
  steps:
  - user: |
      Dentre os documentos necessários, o CPF é um deles para serviços de saúde?
    intent: doc_neces
  - action: action_create_query
  
- story: question 55
  steps:
  - user: |
      Cpf é necessario para serviços de saúde.
    intent: doc_neces
  - action: action_create_query
  
- story: question 56
  steps:
  - user: |
      Os serviços de saúde precisam do cpf para serem realizados?
    intent: doc_neces
  - action: action_create_query
  
- story: question 57
  steps:
  - user: |
      O cpf é um requisito comum aos serviços de saúde?
    intent: doc_neces
  - action: action_create_query
  
- story: question 58
  steps:
  - user: |
      Quanto a categoria saúde, o cpf é um requisito comum para requer?
    intent: doc_neces
  - action: action_create_query
  
- story: question 59
  steps:
  - user: |
      Ao requer um serviço de saúde, é preciso do CPF?
    intent: doc_neces
  - action: action_create_query """