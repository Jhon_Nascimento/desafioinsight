import html5lib
#from html5lib.filters import sanitizer

with open("./OntoService_rdfa.html", "rb") as fp:
    dom = html5lib.parse(fp, treebuilder="dom")
    walker = html5lib.getTreeWalker("dom")
    stream = walker(dom)
    #clean_stream = sanitizer.Filter(stream)
    s = html5lib.serializer.HTMLSerializer()
    output = s.serialize(stream)
    for item in output:
        print("%r" % item)